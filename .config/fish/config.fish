if status is-interactive
    # Commands to run in interactive sessions can go here
end

fish_add_path /Users/asand/.cargo/bin
fish_add_path /Users/asand/Library/Python/3.9/bin
fish_add_path /Users/asand/scripts
fish_add_path /usr/local/sbin
alias v=nvim
alias c=clifm
